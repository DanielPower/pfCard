# Pathfinder Stat Cards

## Purpose
This tool pulls creature stats and information from the Pathfinder Reference 
Document (PRD), and formats it in a space-saving design so you can print your
creature stats on 3x5 or 4x6 index cards.

## Usage Examples
Pull the Goblin stat block  
`python getCreature.py goblin`

Pull a Golem stat block  
`python getCreature.py golem`

Since there is more than one type of golem, you will be promped to select one.

Pull a specific type of golem  
`python getCreature.py golem -t "Flesh Golem"`

## Index Card Example
![Index Card](example.jpg)
