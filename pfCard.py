#!/usr/bin/env python3

import urllib.request
import argparse
import sys
import os


def getPage(creature, bestiary='bestiary'):
    url = 'http://paizo.com/pathfinderRPG/prd/'+bestiary+'/'+creature+'.html'

    try:
        response = urllib.request.urlopen(url)
        html = bytes(response.read())
        html = html.decode("utf-8", "strict")
        html = html.replace('\t', '')
        html = html.replace('\n\n', '\n')
        html = html.split('\n')
    except urllib.error.HTTPError as error:
        print("HTTP Error: "+error.reason)
        sys.exit()
    except urllib.error.URLError as error:
        print("URL Error: "+error.reason)

    return html


def getName(titleLine):
    titleLine = titleLine.replace('<b>', '')
    begin = titleLine.find('title">') + 7
    end = titleLine.find('<span')

    return titleLine[begin:end].strip()


def getTypes(page):
    types = []
    for index, line in enumerate(page):
        if 'stat-block-cr' in line:
            typeName = getName(line)
            types.append({'line': index, 'name': typeName})

    return types


def askType(types):
    print("Please specify a creature type")
    for index, creatureType in enumerate(types):
        print(str(index)+' '+creatureType['name'])
    selection = input('-> ')
    try:
        index = int(selection)
    except ValueError:
        print("Error: Invalid Selection")
        sys.exit()
    return types[index]


def getCreature(page, subType):
    index = subType['line']+1
    creature = []
    creature.append(page[index-1])
    creature.append(page[index-2])
    inSection = False
    for line in page[index:]:
        if 'stat-block-breaker' in line:
            if inSection:
                creature.append('</div>')
            creature.append('<div class="dontbreak">')
            inSection = True
        if line[0:3] != '<p ':
            if inSection:
                inSection = False
                creature.append('</div>')
        if line[0:2] != '<p':
            break
        creature.append(line)

    return creature


def makeCard(creature):
    template = open('template.html', 'r').read().splitlines()
    card = ''
    for line in template:
        if line.strip() != '<!-- INSERT HERE -->':
            card += line + '\n'
        else:
            for line in creature:
                card += line + '\n'

    return card


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Get stat blocks from Pathfinder Database"
    )
    parser.add_argument('creature')
    parser.add_argument('-t', '--subType', default=None, required=False)
    parser.add_argument('-b', '--bestiary', default='bestiary', required=False)
    args = parser.parse_args()

    page = getPage(args.creature, args.bestiary)
    types = getTypes(page)
    if args.subType:
        for subType in types:
            if subType.upper() == args.subType.upper():
                selection = subType
                break
            print("subType not found")
            sys.exit()
    else:
        if len(types) > 1:
            selection = askType(types)
        else:
            selection = types[0]
    creature = getCreature(page, selection)
    card = makeCard(creature)

    dirname = args.creature.title()
    filename = selection['name'].title()

    os.makedirs('creatures', exist_ok=True)
    os.makedirs('creatures/'+dirname, exist_ok=True)
    file = open('creatures/'+dirname+'/'+filename+'.html', 'w')
    file.write(card)
